const validateString = string => !(!string);
const validateNumber = number => !(!number || isNaN(number)); 

const getYourGrades = (message = "Enter your number", validator = validateNumber) => {
    let userNumber;
    do {
        userNumber = prompt(message);
    } while (!validator(userNumber) )

    return +userNumber;
}

const getUserInfo = (message = "Enter your name") => {
    let userName;
    do {
        userName = prompt(message);
    } while (!validateString(userName))
    return userName;
}

const student = {
    name: getUserInfo("Введіть ім'я студента"),
    lastName: getUserInfo("Введіть призвіще студента"),
    tabel: {},

    getGrades() {
      do {
        let subject = prompt('Введіть предмет');
        if (subject === null) {
          break;
        }
  
        let grades = prompt('Введіть вашу оцінку');
        if (grades === null) {
          break;
        }
  
        this.tabel[subject] = grades;
      } while(true)
    },

    showBadGrades(obj) {
      
      let count = 0;

      for(let key in obj) {
        if (obj[key] < 4) {
          count++
        } 
      } 
      if (count === 0) {
        alert("Студент переведено на наступний курс");
      }
      console.log(`У студента ${this.name} ${this.lastName} - ${count} оцінок (менше 4)`);

    },

    showAverageGrade(obj) {
      let counter = 0;
      let previousGrade = 0;
      let sumGrade = 0;
      let totalSumGrade;
      
      for (let key in obj) {
        counter++
        previousGrade = obj[key];
        totalSumGrade = +previousGrade + +sumGrade;
        sumGrade = totalSumGrade;
      }

      let average =  totalSumGrade / counter;
      console.log(average)

      if (average > 7) {
        alert(`Студенту призначено стипендію`);
      }
    }
    
}

// console.log(student);
// console.log(student.getGrades());
// console.log(student.showBadGrades(student.tabel));
// console.log(student.showAverageGrade(student.tabel));

console.log(student);
student.getGrades();
student.showBadGrades(student.tabel);
student.showAverageGrade(student.tabel);




